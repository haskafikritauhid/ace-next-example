import * as React from "react";
import {
  Layout,
  CodeEditor,
  CodeEditorPanel,
  CodeResult,
  Button,
  Text
} from "../components";
import { buildStringExpression } from "../utils";

type Props = {};
type State = {
  currentIndex: number;
  code: string;
  result: Array<number> | number | null;
};

class Index extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      currentIndex: 0,
      code: '',
      result: null
    };
  }

  _handleCodeChange = (val: string) => {
    this.setState({ code: val });
  };

  _handleRun = () => {
    this.setState({ result: eval(this.state.code) });
  };

  _handleCalculate = (name: "sum" | "multiply" | "prime" | "fibonaci") => {
    this.setState({ code: buildStringExpression(name) });
  };

  render() {
    const { currentIndex, code, result } = this.state;
    return (
      <Layout maxWidth="500px">
        <CodeEditorPanel
          currentIndex={currentIndex}
          onCalculate={this._handleCalculate}
        />
        <div className="code-container">
          <CodeEditor
            onChange={this._handleCodeChange}
            width="100%"
            code={code}
          />
          <div className="row__footer">
            <Button onClick={this._handleRun}>
              <i style={{ marginRight: 6 }}>▶</i>
              <Text>Run</Text>
            </Button>
          </div>
        </div>
        <div className="row__result">
          <CodeResult result={result} />
        </div>
        <style jsx>{`
          .code-container {
            position: relative;
          }
          .row__footer {
            box-sizing: border-box;
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            padding: 20px;
            display: flex;
            justify-content: flex-end;
          }
          .row__result {
            margin-top: 20px;
          }
        `}</style>
      </Layout>
    );
  }
}

export default Index;
