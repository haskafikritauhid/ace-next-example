import {
    sum,
    multiply,
    primeNumber,
    fibonaci,
    buildStringExpression
} from "../utils";

describe("utils", () => {
    test("should return exact sum", () => {
        expect(sum([1, 2])).toBe(3);
        expect(sum([1, 2, 3])).toBe(6);
        expect(sum([1, -2, 3])).toBe(2);
    });

    test("should handle sum with null, empty, undefined item", () => {
        expect(sum([])).toBe(null);
        expect(sum(null)).toBe(null);
        expect(sum(undefined)).toBe(null);
    });

    test("should return exact multiply", () => {
        expect(multiply([1, 2])).toBe(2);
        expect(multiply([1, 2, 3])).toBe(6);
    });

    test("should handle multiply with null, empty, undefined item", () => {
        expect(multiply([])).toBe(null);
        expect(multiply(null)).toBe(null);
        expect(multiply(undefined)).toBe(null);
    });

    test("should return primeNumber", () => {
        expect(primeNumber(0)).toEqual([]);
        expect(primeNumber(2)).toEqual([2, 3]);
        expect(primeNumber(4)).toEqual([2, 3, 5, 7]);
    });

    test("fibonaci", () => {
        expect(primeNumber(0)).toEqual([]);
        expect(fibonaci(4)).toEqual([0, 1, 1, 2]);
        expect(fibonaci(8)).toEqual([0, 1, 1, 2, 3, 5, 8, 13]);
    });
});
