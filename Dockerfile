FROM mhart/alpine-node

ARG PACKAGE_PATH=
ARG WORKING_DIR=

WORKDIR ${WORKING_DIR}

COPY ${PACKAGE_PATH} ${WORKING_DIR}

RUN yarn install && yarn build

CMD [ "yarn", "start" ]
