export const sumString = `// SUM\n\nfunction sum(ar) {\n  if (!ar || ar.length <= 0) return null;\n\n  return ar.reduce((a, b) => a + b);\n}\n\nconst ar = [1, 2, 3 ,4];\n\nsum(ar);`;

export const multiplyString = `// Multiply\n\nfunction multiply(ar) {\n  if (!ar || ar.length <= 0) return null;\n\n  return ar.reduce((a, b) => a * b);\n}\n\nconst ar = [1, 2, 3 ,4];\n\nmultiply(ar);`;

export const primeNumberString = `function primeNumber(n) {\n  let primes = [2, 3];\n  let acc = 4;\n\n  if (n === 0) return [];\n\n  while (primes.length < n) {\n    if (acc % 2 === 1) primes.push(acc);\n    acc += 1;\n  }\n\n  return primes;\n}\n\nconst n = 10;\n\nprimeNumber(n);`

export const fibonaciString = `function fibonaci(n) {\n  let sequence = [0, 1, 1];\n  let i = 2;\n\n  while (sequence.length < n) {\n    sequence.push(sequence[i] + sequence[i - 1]);\n    i += 1;\n}\n\n  return sequence;\n}\n\nconst n = 1000;\n\nfibonaci(n);`
