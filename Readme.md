# Submission

Hi, this is a really simple example next + ace editor. How you can run this example:

## running locally  

```sh
# clone the repo url
yarn && yarn dev
```

## unit test

```sh
# clone the repo url
yarn test
```

## docker
```
docker-compose up
```

## misc
- no selenium test yet