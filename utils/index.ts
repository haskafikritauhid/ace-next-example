import { sumString, multiplyString, primeNumberString, fibonaciString } from '../constants'

export function sum(ar: Array<number>): number {
    if (!ar || ar.length <= 0) return null;
    return ar.reduce((a, b) => a + b);
}

export function multiply(ar: Array<number>): number {
    if (!ar || ar.length <= 0) return null;
    return ar.reduce((a, b) => a * b);
}

export function primeNumber(n: number): Array<number> | [] {
    let primes = [2, 3];
    let acc = 4;

    if (n === 0) return [];

    while (primes.length < n) {
        if (acc % 2 === 1) primes.push(acc);
        acc += 1;
    }

    return primes;
}

export function fibonaci(n: number): Array<number> {
    let sequence: Array<number> = [0, 1, 1];
    let i = 2;

    while (sequence.length < n) {
        sequence.push(sequence[i] + sequence[i - 1]);
        i += 1;
    }

    return sequence;
}

export function buildStringExpression(
    name: "sum" | "multiply" | "prime" | "fibonaci"
): string {

    switch (name) {
        case "sum":
            return sumString;
        case "multiply":
            return multiplyString
        case "prime":
            return primeNumberString;
        case "fibonaci":
            return fibonaciString;
        default:
            return "";
    }
}
