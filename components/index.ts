import dynamic from "next/dynamic";
import Layout from "./Layout";
import Touchable from "./Touchable";
import Button from "./Button";
import Text from "./Text";
import CodeEditorPanel from "./CodeEditorPanel";
import CodeResult from "./CodeResult";

const CodeEditor = dynamic(import("./CodeEditor"), { ssr: false });

export {
  CodeEditor,
  Layout,
  Touchable,
  Text,
  Button,
  CodeEditorPanel,
  CodeResult
};
