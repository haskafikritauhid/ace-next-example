import * as React from "react";

type LayoutProps = {
  children: React.ReactNode;
  maxWidth: string;
};

const Layout: React.SFC<LayoutProps> = (props: LayoutProps) => {
  const { children, maxWidth } = props;
  return (
    <div className="layout">
      {children}
      <style jsx>{`
        .layout {
          position: relative;
          max-width: ${maxWidth};
          margin: 0 auto;
          padding-top: 20vh;
        }
      `}</style>
    </div>
  );
};

Layout.defaultProps = {
  maxWidth: "300px"
};

export default Layout;
