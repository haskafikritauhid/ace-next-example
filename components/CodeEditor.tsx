import * as React from 'react';
import brace from 'brace';
import AceEditor from 'react-ace';

import 'brace/mode/javascript';
import 'brace/theme/dracula';


type CodeEditorProps = {
  code: string,
  onChange: (val: string) => void,
  height?: string,
  width?: string,
  initialValue?: string
}

const CodeEditor: React.SFC<CodeEditorProps> = (props: CodeEditorProps) => {
  const { initialValue, height, width, code, onChange } = props
  return (
    <AceEditor
      value={code}
      mode="javascript"
      theme="dracula"
      onChange={(val) => onChange(val)}
      defaultValue={initialValue}
      name="UNIQUE_ID_OF_DIV"
      width={width}
      height={height}
      editorProps={{ $blockScrolling: true }}
    />
  )
}

CodeEditor.defaultProps = {
  width: '300px',
  height: '300px',
  initialValue: `// name: haska fikri tauhid\n//mail: haskafikritauhid@gmail.com\n\n// please select either sum | multiply | prime number | fibonaci number\n// Or, you can play around in this editor
`
}

export default CodeEditor;


