import * as React from "react";

type TextProps = {
  children: React.ReactNode;
  padding?: string;
  fit?: boolean;
};

const Text: React.SFC<TextProps> = (props: TextProps) => {
  const { fit, padding, children } = props;
  return (
    <React.Fragment>
      <span className="text">{children}</span>
      <style jsx>{`
        .text {
          padding: ${padding};
          display: ${fit ? "inline-block" : "block"};
          color: inherit;
        }
      `}</style>
    </React.Fragment>
  );
};

Text.defaultProps = {
  padding: "0",
  fit: true
};

export default Text;
