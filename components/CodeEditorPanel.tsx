import * as React from "react";
import { Text, Touchable } from ".";

type CodeEditorPanelProps = {
  currentIndex?: number;
  onCalculate: (name: string) => void;
};

const CodeEditorPanel: React.SFC<CodeEditorPanelProps> = (
  props: CodeEditorPanelProps
) => {
  const { onCalculate } = props;
  return (
    <div className="code-editor__panel">
      <Touchable onPress={() => onCalculate("sum")}>
        <Text padding="10px 10px 10px 20px">Sum</Text>
      </Touchable>
      <Touchable onPress={() => onCalculate("multiply")}>
        <Text padding="10px">Multiply</Text>
      </Touchable>
      <Touchable onPress={() => onCalculate("prime")}>
        <Text padding="10px">Prime Number</Text>
      </Touchable>
      <Touchable onPress={() => onCalculate("fibonaci")}>
        <Text padding="10px">Fibonaci Number</Text>
      </Touchable>

      <style jsx>{`
        .code-editor__panel {
          background: #575a5a4d;
          border-top-left-radius: 8px;
          border-top-right-radius: 8px;
        }
      `}</style>
    </div>
  );
};

CodeEditorPanel.defaultProps = {
  currentIndex: 0
};

export default CodeEditorPanel;
