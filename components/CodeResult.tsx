import * as React from 'react';

type CodeResultProps = {
  result: Array<number> | number | null
}

const CodeResult: React.SFC<CodeResultProps> = (props: CodeResultProps) => {
  const { result } = props
  if (!result) return null

  return (
    <pre className="code-result">
      Result = {Array.isArray(result) ? renderList(result) : result}
      <style jsx>
        {`.code-result {font-size: 16px; white-space: normal;}`}
      </style>
    </pre>
  )
}

const renderList = (result: Array<number>) => (
  <code>
    {result.map((n, i) => <code key={`res-${i}`}>{`${n}, `}</code>)}
  </code>
)

export default CodeResult
