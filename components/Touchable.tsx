import * as React from "react";

type TouchableProps = {
  children: React.ReactNode;
  onPress: () => void;
};

const Touchable: React.SFC<TouchableProps> = (props: TouchableProps) => {
  return (
    <div className="touch" onClick={() => props.onPress()}>
      {props.children}
      <style jsx>{`
        .touch {
          cursor: pointer;
          display: inline-block;
        }
        .touch:hover {
          opacity: 0.7;
        }
      `}</style>
    </div>
  );
};

export default Touchable;
