import * as React from "react";

type ButtonProps = {
  children: React.ReactNode;
  type?: "button" | "submit";
  onClick: (e: React.SyntheticEvent) => void;
};

const Button: React.SFC<ButtonProps> = (props: ButtonProps) => {
  const { children, onClick } = props;
  return (
    <button className="btn" onClick={e => onClick(e)}>
      {children}
      <style jsx>{`
        .btn {
          background-color: #7ca4e3;
          border: 0;
          width: auto;
          cursor: pointer;
          display: flex;
          align-items: center;
          font-weight: normal;
          font-size: 13px;
          text-transform: uppercase;
          line-height: 1;
          padding: 9px 10px 7px;
          position: relative;
          text-align: center;
          border-radius: 4px;
          border: 1px solid #6795de;
          color: #131414;
          outline: none;
        }
        .btn:hover {
          opacity: 0.7;
        }
      `}</style>
    </button>
  );
};

Button.defaultProps = {
  type: "button"
};

export default Button;
